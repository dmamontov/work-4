#!/usr/bin/env bash

lockfile=/tmp/lockfile

# some user
mail_resiver=mail_resiver

#some files
tmp_file=/tmp/secure_report.tmp
report_file=/tmp/secure_report

function set_empty_file {
	#create and check reposrts file
	if touch $1
	then
		date > $1
	else 
		echo "Error with file $1" >> /dev/stderr
	fi
}

function get_top_users {
	# top 10 users by day
        echo " top 10 users by day" >> $report_file
        awk '{ print $4 }' $tmp_file | uniq -c | sort -k1 | tail -n 10 >> $report_file
}

function get_connect_ssh {
	# some check ssh port
	echo " top ip and errors connect to ssh" >> $report_file
        grep "error: Received disconnect from" $tmp_file | awk '{ print $10 }' | uniq -c | sort -r >> $report_file
}

function check_hamsters {
	# hamsters info
	echo "Hamsters info" >> $report_file
	for i in $(egrep x:[1-9]{1}[0-9]{3}  /etc/passwd | grep -v "Anonymous NFS User" | awk -F: '{ print $6 }'); 
	do 
		du -sh $i >> $report_file
	done
}

if ( set -o noclobber; echo "$$" > "$lockfile") 2> /dev/null;
then 
	trap 'rm -f "$lockfile"; exit $?' INT TERM EXIT KILL   
# paste code this

	set_empty_file $tmp_file

	#checking current day 
	awk -v day="$(date +%d)" '$2 ==  day' /var/log/secure > $tmp_file
	
	set_empty_file $report_file

	# top 10 users by day
	get_top_users

	# some check ssh port
	get_connect_ssh
	
	# check hamsters
	check_hamsters	

	#send mail
	cat /tmp/secure_report | mail -s "Secure script report" $mail_resiver

	rm $tmp_file
	
# end code
	rm -f "$lockfile" 
	trap - INT TERM EXIT
else  
	echo "Failed to acquire lockfile: $lockfile."  
	echo "Held by $(cat $lockfile)" 
fi
